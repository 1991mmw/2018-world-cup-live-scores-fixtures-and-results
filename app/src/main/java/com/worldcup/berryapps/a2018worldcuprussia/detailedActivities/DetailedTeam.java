package com.worldcup.berryapps.a2018worldcuprussia.detailedActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.worldcup.berryapps.a2018worldcuprussia.FlagThumbnailProcessor;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.LiveTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.UpcomingTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.model.Fixture;
import com.worldcup.berryapps.a2018worldcuprussia.token.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DetailedTeam extends AppCompatActivity {

    private TextView teamNameView;
    private TextView groupNameView;
    private ImageView thumbnail;
    private String teamName;
    private String fixturesUrlPage1 = "https://api.sportdeer.com/v1/seasons/296/fixtures?page=1&access_token=";
    private String fixturesUrlPage2 = "https://api.sportdeer.com/v1/seasons/296/fixtures?page=2&access_token=";
    private String fixturesUrlPage3 = "https://api.sportdeer.com/v1/seasons/296/fixtures?page=3&access_token=";
    private List<String> fixturesUrl = new ArrayList<>();
    private String URL_TO_HIT;
    private ListView listTeamMatches;
    private ImageView backButton;
    private final String refreshToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YWZjMmYyMDMyZGI0NTY5Njc0MDMxN2IiLCJpYXQiOjE1MjgxODgzMjd9.35xB8Gq8GXrvrSZyFDElnGGgblpkOwEJnDO8KRdhvhw";
    private UpcomingTabFragment.OnFragmentInteractionListener mListener;
    private String accessToken;
    private List<Fixture> collect = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_team);
        // Showing and Enabling clicks on the Home/Up button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        listTeamMatches = findViewById(R.id.listTeamMatches);
        // setting up text views and stuff
        setUpUIViews();
        backButton.setOnClickListener(v -> finish());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            teamName = bundle.getString("teamName"); // getting the model from MainActivity send via extras
            String group = bundle.getString("group"); // getting the model from MainActivity send via extras
            String thumbnail_string = bundle.getString("thumbnail"); // getting the model from MainActivity send via extras

            teamNameView.setText(teamName);
            groupNameView.setText(group);
            thumbnail.setImageResource(processTeamThumbnail(teamName));


        }

        new JSONTask().execute(URL_TO_HIT);
    }

    private int processTeamThumbnail(String team) {
        if (team.equals("Argentina")) {
            return R.drawable.flag0argentina;
        }
        if (team.equals("Australia")) {
            return R.drawable.flag0australia;
        }
        if (team.equals("Belgium")) {
            return R.drawable.flag0belgium;
        }
        if (team.equals("Brazil")) {
            return R.drawable.flag0brazil;
        }
        if (team.equals("Colombia")) {
            return R.drawable.flag0colombia;
        }
        if (team.equals("Costa Rica")) {
            return R.drawable.flag0costa_rica;
        }
        if (team.equals("Croatia")) {
            return R.drawable.flag0croatia;
        }
        if (team.equals("Denmark")) {
            return R.drawable.flag0denmark;
        }
        if (team.equals("Egypt")) {
            return R.drawable.flag0egypt;
        }
        if (team.equals("England")) {
            return R.drawable.flag0england;
        }
        if (team.equals("France")) {
            return R.drawable.flag0france;
        }
        if (team.equals("Germany")) {
            return R.drawable.flag0germany;
        }
        if (team.equals("Iceland")) {
            return R.drawable.flag0iceland;
        }
        if (team.equals("Iran")) {
            return R.drawable.flag0iran;
        }
        if (team.equals("Japan")) {
            return R.drawable.flag0japan;
        }
        if (team.equals("Mexico")) {
            return R.drawable.flag0mexico;
        }
        if (team.equals("Morocco")) {
            return R.drawable.flag0morocco;
        }
        if (team.equals("Nigeria")) {
            return R.drawable.flag0nigeria;
        }
        if (team.equals("Panama")) {
            return R.drawable.flag0panama;
        }
        if (team.equals("Peru")) {
            return R.drawable.flag0peru;
        }
        if (team.equals("Poland")) {
            return R.drawable.flag0poland;
        }
        if (team.equals("Portugal")) {
            return R.drawable.flag0portugal;
        }
        if (team.equals("Russia")) {
            return R.drawable.flag0russia;
        }
        if (team.equals("Saudi Arabia")) {
            return R.drawable.flag0saudi_arabia;
        }
        if (team.equals("Senegal")) {
            return R.drawable.flag0senegal;
        }
        if (team.equals("Serbia")) {
            return R.drawable.flag0serbia;
        }
        if (team.equals("South Korea")) {
            return R.drawable.flag0south_korea;
        }
        if (team.equals("Spain")) {
            return R.drawable.flag0spain;
        }
        if (team.equals("Sweden")) {
            return R.drawable.flag0sweden;
        }
        if (team.equals("Switzerland")) {
            return R.drawable.flag0switzerland;
        }
        if (team.equals("Tunisia")) {
            return R.drawable.flag0tunisia;
        }
        if (team.equals("Uruguay")) {
            return R.drawable.flag0uruguay;
        }
        return R.drawable.cover_2;
    }

    private void setUpUIViews() {
        teamNameView = (TextView) findViewById(R.id.textView);
        groupNameView = (TextView) findViewById(R.id.textView2);
        thumbnail = (ImageView) findViewById(R.id.imageView2);
        backButton = findViewById(R.id.backButton);
    }

    public class JSONTask extends AsyncTask<String, String, List<Fixture>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fixturesUrl.clear();
            collect.clear();
        }

        @Override
        protected List<Fixture> doInBackground(String... params) {
            List<Fixture> fixtureList = new ArrayList<>();

            fixturesUrl.add(fixturesUrlPage1);
            fixturesUrl.add(fixturesUrlPage2);
            fixturesUrl.add(fixturesUrlPage3);

            for (int i = 0; i < fixturesUrl.size(); i++) {
                HttpURLConnection connection = null;
                BufferedReader reader = null;

                //Get Access Token
                accessToken = "https://api.sportdeer.com/v1/accessToken?refresh_token=" + refreshToken;
                AccessToken at = new AccessToken();
                accessToken = at.createAccessToken(accessToken);
                URL_TO_HIT = fixturesUrl.get(i) + accessToken;


                try {
                    URL url = new URL(URL_TO_HIT);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    InputStream stream = connection.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    String finalJson = buffer.toString();

                    JSONObject parentObject = new JSONObject(finalJson);
                    JSONArray parentArray = parentObject.getJSONArray("docs");


                    Gson gson = new Gson();
                    for (int j = 0; j < parentArray.length(); j++) {
                        JSONObject finalObject = parentArray.getJSONObject(j);

                        Fixture fixture = gson.fromJson(finalObject.toString(), Fixture.class);
                        fixtureList.add(fixture);
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            collect = fixtureList.stream().sorted(Comparator.comparing(Fixture::getSchedule_date)).distinct().collect(Collectors.toList());
            return collect;
        }

        @Override
        protected void onPostExecute(final List<Fixture> result) {
            super.onPostExecute(result);
            if (result != null) {
                List<Fixture> upcomingFixtures = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i).getTeam_season_home_name().equals(teamName) || result.get(i).getTeam_season_away_name().equals(teamName)) {
                        upcomingFixtures.add(result.get(i));
                    }
                }
                DetailedTeam.FixtureAdapter adapter = new DetailedTeam.FixtureAdapter(getApplicationContext(), R.layout.row, upcomingFixtures);
                listTeamMatches.setAdapter(adapter);

                listTeamMatches.setOnItemClickListener((parent, view1, position, id) -> {
                    listTeamMatches.setEnabled(false);
                    listTeamMatches.setClickable(false);
                    Fixture fixture = upcomingFixtures.get(position);
                    Intent intent = new Intent(getApplicationContext(), DetailedMatch.class);
                    intent.putExtra("awayTeam", fixture.getTeam_season_away_name());
                    intent.putExtra("homeTeam", fixture.getTeam_season_home_name());
                    intent.putExtra("awayScore", fixture.getNumber_goal_team_away());
                    intent.putExtra("homeScore", fixture.getNumber_goal_team_home());
                    intent.putExtra("date", fixture.getSchedule_date());
                    intent.putExtra("stadiumName", fixture.getStadium());
                    intent.putExtra("fixtureStatus", fixture.getFixture_status_short());
                    startActivity(intent);
                });
            } else {
                Toast.makeText(getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class FixtureAdapter extends ArrayAdapter {

        private List<Fixture> fixtureList;
        private int resource;
        private LayoutInflater inflater;

        public FixtureAdapter(Context context, int resource, List<Fixture> objects) {
            super(context, resource, objects);
            fixtureList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            DetailedTeam.FixtureAdapter.ViewHolder holder = null;

            if (convertView == null) {
                holder = new DetailedTeam.FixtureAdapter.ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.awayTeam = (TextView) convertView.findViewById(R.id.awayTeam);
                holder.homeTeam = (TextView) convertView.findViewById(R.id.homeTeam);
                holder.awayTeamThumbnail = (ImageView) convertView.findViewById(R.id.awayTeamThumbnail);
                holder.homeTeamThumbnail = (ImageView) convertView.findViewById(R.id.homeTeamThumbnail);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.stadium = (TextView) convertView.findViewById(R.id.stadium);
                holder.scoreAwayTeam = (TextView) convertView.findViewById(R.id.textViewScoreAway);
                holder.scoreHomeTeam = (TextView) convertView.findViewById(R.id.textViewScoreHome);
                convertView.setTag(holder);
            } else {
                holder = (DetailedTeam.FixtureAdapter.ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final DetailedTeam.FixtureAdapter.ViewHolder finalHolder = holder;

            String dateString = fixtureList.get(position).getSchedule_date().toString();
            FlagThumbnailProcessor ftp = new FlagThumbnailProcessor();
            String parsedDate = ftp.parseDateTime(dateString);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/dusha.ttf");
            holder.homeTeam.setText(fixtureList.get(position).getTeam_season_home_name().toUpperCase());
            holder.homeTeam.setTypeface(type);
            holder.awayTeam.setTypeface(type);
            holder.awayTeam.setText(fixtureList.get(position).getTeam_season_away_name().toUpperCase());
            holder.date.setText("Kick Off: " + parsedDate);
            holder.stadium.setText("Stadium: " + fixtureList.get(position).getStadium());

            holder.awayTeamThumbnail.setImageResource(ftp.processTeamThumbnail(fixtureList.get(position).getTeam_season_away_name()));
            holder.homeTeamThumbnail.setImageResource(ftp.processTeamThumbnail(fixtureList.get(position).getTeam_season_home_name()));
            if (!fixtureList.get(position).getFixture_status_short().equals("NS")) {
                holder.scoreHomeTeam.setText(fixtureList.get(position).getNumber_goal_team_home());
                holder.scoreAwayTeam.setText(fixtureList.get(position).getNumber_goal_team_away());
            }
            return convertView;
        }

        class ViewHolder {
            private ImageView awayTeamThumbnail;
            private ImageView homeTeamThumbnail;
            private LinearLayout linearLayout;
            private TextView fixtureStatus;
            private TextView date;
            private TextView awayTeam;
            private TextView homeTeam;
            private TextView round;
            private TextView stadium;
            private TextView scoreHomeTeam;
            private TextView scoreAwayTeam;
        }

    }
}
