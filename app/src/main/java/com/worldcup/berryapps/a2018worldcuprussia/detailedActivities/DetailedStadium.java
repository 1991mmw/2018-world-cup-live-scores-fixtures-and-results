package com.worldcup.berryapps.a2018worldcuprussia.detailedActivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.worldcup.berryapps.a2018worldcuprussia.FlagThumbnailProcessor;
import com.worldcup.berryapps.a2018worldcuprussia.R;

public class DetailedStadium extends AppCompatActivity {

    private TextView cityView;
    private TextView capacityView;
    private TextView stadiumName;
    private ImageView stadiumImage;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_stadium);

        // Showing and Enabling clicks on the Home/Up button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // setting up text views and stuff
        setUpUIViews();

        backButton.setOnClickListener(v -> {
            finish();
        });
        // recovering data from MainActivity, sent via intent
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String city = bundle.getString("location"); // getting the model from MainActivity send via extras
            String capacity = bundle.getString("capacity"); // getting the model from MainActivity send via extras
            String name = bundle.getString("title"); // getting the model from MainActivity send via extras


            capacityView.setText(capacity);
            cityView.setText(city);
            stadiumName.setText(name);
            FlagThumbnailProcessor ftp = new FlagThumbnailProcessor();
            stadiumImage.setImageResource(ftp.processStadiumImage(name));

        }

    }

    private void setUpUIViews() {
        cityView = (TextView) findViewById(R.id.city);
        capacityView = (TextView) findViewById(R.id.capacity);
        stadiumName = (TextView) findViewById(R.id.stadium_name);
        stadiumImage = (ImageView) findViewById(R.id.stadiumView);
        backButton = findViewById(R.id.backButton);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}