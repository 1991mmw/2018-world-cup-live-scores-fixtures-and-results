package com.worldcup.berryapps.a2018worldcuprussia.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.detailedActivities.DetailedStadium;
import com.worldcup.berryapps.a2018worldcuprussia.model.Stadium;

import java.util.List;

public class StadiumsAdapter extends RecyclerView.Adapter<StadiumsAdapter.MyViewHolder>  {

    private Context mContext;
    private List<Stadium> stadiumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, group, subtitle;
        public ImageView thumbnail;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            cardView = (CardView) view.findViewById(R.id.card_view);
        }
    }


    public StadiumsAdapter(Context mContext, List<Stadium> stadiumList) {
        this.mContext = mContext;
        this.stadiumList = stadiumList;
    }

    @Override
    public StadiumsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_card, parent, false);



        return new StadiumsAdapter.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(final StadiumsAdapter.MyViewHolder holder, int position) {
        Stadium stadium = stadiumList.get(position);
        holder.title.setText(stadium.getName());
        String capacity = stadium.getCapacity();
        String location = stadium.getLocation();
        String title = stadium.getName();

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, DetailedStadium.class);
            intent.putExtra("capacity", capacity);
            intent.putExtra("location", location);
            intent.putExtra("title", title);
            mContext.startActivity(intent);
        });

        // loading stadium cover using Glide library
        Glide.with(mContext).load(stadium.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return stadiumList.size();
    }
}
