package com.worldcup.berryapps.a2018worldcuprussia.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.detailedActivities.DetailedTeam;
import com.worldcup.berryapps.a2018worldcuprussia.model.Team;

import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.MyViewHolder> {

    private Context mContext;
    private List<Team> teamList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, group;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);
        }
    }


    public TeamAdapter(Context mContext, List<Team> teamList) {
        this.mContext = mContext;
        this.teamList = teamList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Team team = teamList.get(position);
        holder.title.setText(team.getName());

        holder.overflow.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, DetailedTeam.class);
            intent.putExtra("teamName", team.getName());
            intent.putExtra("group", team.getGroup());
            intent.putExtra("thumbnail", team.getThumbnail());
            mContext.startActivity(intent);
        });

        holder.thumbnail.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, DetailedTeam.class);
            intent.putExtra("teamName", team.getName());
            intent.putExtra("group", team.getGroup());
            intent.putExtra("thumbnail", team.getThumbnail());
            mContext.startActivity(intent);
        });
        // loading team cover using Glide library
        Glide.with(mContext).load(team.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }
}
