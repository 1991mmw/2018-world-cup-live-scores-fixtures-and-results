package com.worldcup.berryapps.a2018worldcuprussia.groupsTabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.StandingsAdapter;
import com.worldcup.berryapps.a2018worldcuprussia.database.Database;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;
import com.worldcup.berryapps.a2018worldcuprussia.parser.StandingsParser;
import com.worldcup.berryapps.a2018worldcuprussia.token.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by User on 2/28/2017.
 */

public class Tab2Fragment extends Fragment {
    private static final String TAG = "Tab2Fragment";
    private ListView listStandings;
    private StandingsParser sp = new StandingsParser();
    private List<Standings> groupB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2_fragment, container, false);
        listStandings = view.findViewById(R.id.listStandingsB);
        List<Standings> standingsList = Database.getDatabase(getContext()).standingsDao().getAll();
        groupB = standingsList.stream()
                .filter(v -> v.getTeam_season_name().equals("Portugal") ||
                        v.getTeam_season_name().equals("Spain") ||
                        v.getTeam_season_name().equals("Morocco") ||
                        v.getTeam_season_name().equals("Iran"))
                .sorted(Comparator.comparing(Standings::getPosition))
                .collect(Collectors.toList());
        if (groupB != null) {
            StandingsAdapter adapter = new StandingsAdapter(getActivity(), R.layout.rows_standings, groupB);
            listStandings.setAdapter(adapter);
        } else {
            Toast.makeText(getContext().getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
        }
        return view;
    }
}
