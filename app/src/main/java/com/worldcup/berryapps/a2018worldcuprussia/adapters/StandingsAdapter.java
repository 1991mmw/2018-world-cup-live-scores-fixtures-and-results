package com.worldcup.berryapps.a2018worldcuprussia.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.worldcup.berryapps.a2018worldcuprussia.FlagThumbnailProcessor;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;

import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class StandingsAdapter extends ArrayAdapter {

        private List<Standings> standingsList;
        private int resource;
        private LayoutInflater inflater;
        public StandingsAdapter(Context context, int resource, List<Standings> objects) {
            super(context, resource, objects);
            standingsList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if(convertView == null){
                holder = new ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.teamFlagThumbnail = (ImageView) convertView.findViewById(R.id.teamFlagThumbnail);
                holder.teamName = convertView.findViewById(R.id.teamName);
                holder.played = (TextView)convertView.findViewById(R.id.played);
                holder.wins = (TextView) convertView.findViewById(R.id.wins);
                holder.losses = (TextView) convertView.findViewById(R.id.losses);
                holder.draws = (TextView) convertView.findViewById(R.id.draws);
                holder.goalsFor = (TextView) convertView.findViewById(R.id.goalsFor);
                holder.goalsAgainst = (TextView) convertView.findViewById(R.id.goalsAgainst);
                holder.points = (TextView) convertView.findViewById(R.id.points);
                holder.goalDifference = (TextView) convertView.findViewById(R.id.goalDifference);
                holder.position = (LinearLayout) convertView.findViewById(R.id.standingsRow);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar)convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final ViewHolder finalHolder = holder;

            if(standingsList.get(position).getPosition().equals("1") || standingsList.get(position).getPosition().equals("2")) {
                holder.position.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.layout_qualify));
            }
            if(standingsList.get(position).getPosition().equals("3") || standingsList.get(position).getPosition().equals("4")) {
                holder.position.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.layout_non_qualify));
            }
            FlagThumbnailProcessor ftp = new FlagThumbnailProcessor();
            holder.teamFlagThumbnail.setImageResource(ftp.processTeamThumbnail(standingsList.get(position).getTeam_season_name()));
            holder.teamName.setText(standingsList.get(position).getTeam_season_name().substring(0,3).toUpperCase());
            holder.played.setText(standingsList.get(position).getP());
            holder.wins.setText(standingsList.get(position).getW_tot());
            holder.losses.setText(standingsList.get(position).getL_tot());
            holder.draws.setText(standingsList.get(position).getD_tot());
            holder.goalsFor.setText(standingsList.get(position).getGf_tot());
            holder.goalsAgainst.setText(standingsList.get(position).getGa_tot());
            holder.goalDifference.setText(standingsList.get(position).getGd_tot());
            holder.points.setText(standingsList.get(position).getPts());

            return convertView;
        }


        class ViewHolder{
            private ImageView teamFlagThumbnail;
            private TextView teamName;
            private TextView played;
            private TextView wins;
            private TextView losses;
            private TextView draws;
            private TextView goalsFor;
            private TextView goalsAgainst;
            private TextView goalDifference;
            private TextView points;
            private LinearLayout position;
        }

    }