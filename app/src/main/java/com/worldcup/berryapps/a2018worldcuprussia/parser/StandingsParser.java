package com.worldcup.berryapps.a2018worldcuprussia.parser;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.google.gson.Gson;
import com.worldcup.berryapps.a2018worldcuprussia.database.Database;
import com.worldcup.berryapps.a2018worldcuprussia.database.StandingsDatabaseInitializer;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;
import com.worldcup.berryapps.a2018worldcuprussia.token.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StandingsParser {

    public List<Standings> groupA;
    public List<Standings> groupB;
    public List<Standings> groupC;
    public List<Standings> groupD;
    public List<Standings> groupE;
    public List<Standings> groupF;
    public List<Standings> groupG;
    public List<Standings> groupH;
    private List<String> standingsUrl = new ArrayList<>();
    private String URL_TO_HIT;
    private final String refreshToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YWZjMmYyMDMyZGI0NTY5Njc0MDMxN2IiLCJpYXQiOjE1MjgxODgzMjd9.35xB8Gq8GXrvrSZyFDElnGGgblpkOwEJnDO8KRdhvhw";
    private String accessToken;
    List<Standings> standingsList = new ArrayList<>();
    private Database db;
    private List<Standings> collect = new ArrayList<>();
    private Context mContext;

    public void initJSONTask(Context context) {
        mContext = context;
        new JSONTask().execute(URL_TO_HIT);
    }

    public List<Standings> getGroupA() {
        return groupA;
    }

    public List<Standings> getGroupB() {
        return groupB;
    }

    public List<Standings> getGroupC() {
        return groupC;
    }

    public List<Standings> getGroupD() {
        return groupD;
    }

    public List<Standings> getGroupE() {
        return groupE;
    }

    public List<Standings> getGroupF() {
        return groupF;
    }

    public List<Standings> getGroupG() {
        return groupG;
    }

    public List<Standings> getGroupH() {
        return groupH;
    }

    public class JSONTask extends AsyncTask<String, String, List<Standings>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            standingsUrl.clear();
            collect.clear();
        }

        @Override
        protected List<Standings> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            String groupAStandingsUrl = "https://api.sportdeer.com/v1/stages/1078/standing?access_token=";
            standingsUrl.add(groupAStandingsUrl);
            String groupBStandingsUrl = "https://api.sportdeer.com/v1/stages/1105/standing?access_token=";
            standingsUrl.add(groupBStandingsUrl);
            String groupCStandingsUrl = "https://api.sportdeer.com/v1/stages/1088/standing?access_token=";
            standingsUrl.add(groupCStandingsUrl);
            String groupDStandingsUrl = "https://api.sportdeer.com/v1/stages/1095/standing?access_token=";
            standingsUrl.add(groupDStandingsUrl);
            String groupEStandingsUrl = "https://api.sportdeer.com/v1/stages/1163/standing?access_token=";
            standingsUrl.add(groupEStandingsUrl);
            String groupFStandingsUrl = "https://api.sportdeer.com/v1/stages/1079/standing?access_token=";
            standingsUrl.add(groupFStandingsUrl);
            String groupGStandingsUrl = "https://api.sportdeer.com/v1/stages/1109/standing?access_token=";
            standingsUrl.add(groupGStandingsUrl);
            String groupHStandingsUrl = "https://api.sportdeer.com/v1/stages/1090/standing?access_token=";
            standingsUrl.add(groupHStandingsUrl);

            //Get Access Token
            accessToken = "https://api.sportdeer.com/v1/accessToken?refresh_token=" + refreshToken;
            AccessToken at = new AccessToken();
            accessToken = at.createAccessToken(accessToken);

            for(int i = 0; i < standingsUrl.size(); i++) {

                URL_TO_HIT = standingsUrl.get(i) + accessToken;


                try {
                    URL url = new URL(URL_TO_HIT);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    InputStream stream = connection.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    String finalJson = buffer.toString();

                    JSONObject parentObject = new JSONObject(finalJson);
                    JSONArray parentArray = parentObject.getJSONArray("docs");


                    Gson gson = new Gson();
                    for (int j = 0; j < parentArray.length(); j++) {
                        JSONObject finalObject = parentArray.getJSONObject(j);

                        Standings standings = gson.fromJson(finalObject.toString(), Standings.class);
                        standingsList.add(standings);
                    }

                } catch (MalformedURLException | JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Database.getDatabase(mContext).standingsDao().nukeTable();
            StandingsDatabaseInitializer.populateAsync(Database.getDatabase(mContext), standingsList);
            return collect;
        }

        @Override
        protected void onPostExecute(final List<Standings> result) {
            super.onPostExecute(result);
        }
    }
}
