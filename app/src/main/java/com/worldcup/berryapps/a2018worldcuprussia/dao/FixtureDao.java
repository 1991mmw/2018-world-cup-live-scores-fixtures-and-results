package com.worldcup.berryapps.a2018worldcuprussia.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.worldcup.berryapps.a2018worldcuprussia.model.Fixture;

import java.util.List;

@Dao
public interface FixtureDao {

    @Insert
    void insertAll(Fixture... fixture);

    @Delete
    void delete(Fixture fixture);

    @Query("SELECT * FROM fixture")
    List<Fixture> getAll();

    @Query("DELETE FROM fixture")
    void nukeTable();
}
