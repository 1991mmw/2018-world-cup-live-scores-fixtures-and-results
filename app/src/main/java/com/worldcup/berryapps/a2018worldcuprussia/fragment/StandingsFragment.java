package com.worldcup.berryapps.a2018worldcuprussia.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.SectionsPageAdapter;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab1Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab2Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab3Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab4Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab5Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab6Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab7Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.groupsTabs.Tab8Fragment;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.LiveTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.ResultsTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.UpcomingTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;
import com.worldcup.berryapps.a2018worldcuprussia.parser.StandingsParser;

import java.util.List;

public class StandingsFragment extends Fragment {

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;
    private StandingsParser sp = new StandingsParser();

    @Override
    public void onResume() {
        super.onResume();
        if (getFragmentManager() != null) {
            for (Fragment fragment : getFragmentManager().getFragments()) {
                if (fragment instanceof Tab1Fragment || fragment instanceof Tab2Fragment || fragment instanceof Tab3Fragment
                        || fragment instanceof Tab4Fragment || fragment instanceof Tab5Fragment || fragment instanceof Tab6Fragment
                        || fragment instanceof Tab7Fragment || fragment instanceof Tab8Fragment) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.detach(fragment);
                    ft.attach(fragment);
                    ft.commit();
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main3, container, false);
        mSectionsPageAdapter = new SectionsPageAdapter(getChildFragmentManager());
        sp.initJSONTask(getContext());

        // Set up the ViewPager with the sections adapter.
        mViewPager = view.findViewById(R.id.container);
        setupViewPager(mViewPager);
        TabLayout tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getFragmentManager());
        adapter.addFragment(new Tab1Fragment(), "Group A");
        adapter.addFragment(new Tab2Fragment(), "Group B");
        adapter.addFragment(new Tab3Fragment(), "Group C");
        adapter.addFragment(new Tab4Fragment(), "Group D");
        adapter.addFragment(new Tab5Fragment(), "Group E");
        adapter.addFragment(new Tab6Fragment(), "Group F");
        adapter.addFragment(new Tab7Fragment(), "Group G");
        adapter.addFragment(new Tab8Fragment(), "Group H");
        viewPager.setAdapter(adapter);
    }
}