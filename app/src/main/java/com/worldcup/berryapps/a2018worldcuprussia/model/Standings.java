package com.worldcup.berryapps.a2018worldcuprussia.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "standings")
public class Standings {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "team_season_name")
    private String team_season_name;

    @ColumnInfo(name = "p")
    private String p;

    @ColumnInfo(name = "pts")
    private String pts;

    @ColumnInfo(name = "w_tot")
    private String w_tot;

    @ColumnInfo(name = "l_tot")
    private String l_tot;

    @ColumnInfo(name = "d_tot")
    private String d_tot;

    @ColumnInfo(name = "gf_tot")
    private String gf_tot;

    @ColumnInfo(name = "ga_tot")
    private String ga_tot;

    @ColumnInfo(name = "gd_tot")
    private String gd_tot;

    @ColumnInfo(name = "position")
    private String position;

    @ColumnInfo(name = "thumbnail")
    private String thumbnail;

    @ColumnInfo(name = "group")
    private String group;

    public Standings() {
    }

    public Standings(int uid, String team_season_name, String p, String pts, String w_tot, String l_tot, String d_tot, String gf_tot, String ga_tot, String gd_tot, String position, String thumbnail, String group) {
        this.uid = uid;
        this.team_season_name = team_season_name;
        this.p = p;
        this.pts = pts;
        this.w_tot = w_tot;
        this.l_tot = l_tot;
        this.d_tot = d_tot;
        this.gf_tot = gf_tot;
        this.ga_tot = ga_tot;
        this.gd_tot = gd_tot;
        this.position = position;
        this.thumbnail = thumbnail;
        this.group = group;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTeam_season_name() {
        return team_season_name;
    }

    public void setTeam_season_name(String team_season_name) {
        this.team_season_name = team_season_name;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getPts() {
        return pts;
    }

    public void setPts(String pts) {
        this.pts = pts;
    }

    public String getW_tot() {
        return w_tot;
    }

    public void setW_tot(String w_tot) {
        this.w_tot = w_tot;
    }

    public String getL_tot() {
        return l_tot;
    }

    public void setL_tot(String l_tot) {
        this.l_tot = l_tot;
    }

    public String getD_tot() {
        return d_tot;
    }

    public void setD_tot(String d_tot) {
        this.d_tot = d_tot;
    }

    public String getGf_tot() {
        return gf_tot;
    }

    public void setGf_tot(String gf_tot) {
        this.gf_tot = gf_tot;
    }

    public String getGa_tot() {
        return ga_tot;
    }

    public void setGa_tot(String ga_tot) {
        this.ga_tot = ga_tot;
    }

    public String getGd_tot() {
        return gd_tot;
    }

    public void setGd_tot(String gd_tot) {
        this.gd_tot = gd_tot;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}