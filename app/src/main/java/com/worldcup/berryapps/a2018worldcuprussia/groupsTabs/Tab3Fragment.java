package com.worldcup.berryapps.a2018worldcuprussia.groupsTabs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.StandingsAdapter;
import com.worldcup.berryapps.a2018worldcuprussia.database.Database;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;
import com.worldcup.berryapps.a2018worldcuprussia.parser.StandingsParser;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by User on 2/28/2017.
 */

public class Tab3Fragment extends Fragment {
    private static final String TAG = "Tab3Fragment";
    private ListView listStandings;
    private List<Standings> groupC;
    private StandingsParser sp = new StandingsParser();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab3_fragment, container, false);

        List<Standings> standingsList = Database.getDatabase(getContext()).standingsDao().getAll();
        groupC = standingsList.stream()
                .filter(v -> v.getTeam_season_name().equals("France") ||
                        v.getTeam_season_name().equals("Australia") ||
                        v.getTeam_season_name().equals("Peru") ||
                        v.getTeam_season_name().equals("Denmark"))
                .sorted(Comparator.comparing(Standings::getPosition))
                .collect(Collectors.toList());
        listStandings = view.findViewById(R.id.listStandingsC);
        if (groupC != null) {
            StandingsAdapter adapter = new StandingsAdapter(getActivity(), R.layout.rows_standings, groupC);
            listStandings.setAdapter(adapter);
        } else {
            Toast.makeText(getContext().getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
        }
        return view;
    }
}
