package com.worldcup.berryapps.a2018worldcuprussia.model;

public class Event {

    private String type;
    private String id_fixture;
    private String elapsed;
    private String goal_type_code;
    private String goal_type_desc;
    private String id_team_season;
    private String team_name;
    private String id_team_season_scorer;
    private String card_type;

    public Event() {
    }

    public Event(String type, String id_fixture, String elapsed, String goal_type_code, String goal_type_desc, String id_team_season, String team_name, String id_team_season_scorer, String card_type) {
        this.type = type;
        this.id_fixture = id_fixture;
        this.elapsed = elapsed;
        this.goal_type_code = goal_type_code;
        this.goal_type_desc = goal_type_desc;
        this.id_team_season = id_team_season;
        this.team_name = team_name;
        this.id_team_season_scorer = id_team_season_scorer;
        this.card_type = card_type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId_fixture() {
        return id_fixture;
    }

    public void setId_fixture(String id_fixture) {
        this.id_fixture = id_fixture;
    }

    public String getElapsed() {
        return elapsed;
    }

    public void setElapsed(String elapsed) {
        this.elapsed = elapsed;
    }

    public String getGoal_type_code() {
        return goal_type_code;
    }

    public void setGoal_type_code(String goal_type_code) {
        this.goal_type_code = goal_type_code;
    }

    public String getGoal_type_desc() {
        return goal_type_desc;
    }

    public void setGoal_type_desc(String goal_type_desc) {
        this.goal_type_desc = goal_type_desc;
    }

    public String getId_team_season() {
        return id_team_season;
    }

    public void setId_team_season(String id_team_season) {
        this.id_team_season = id_team_season;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getId_team_season_scorer() {
        return id_team_season_scorer;
    }

    public void setId_team_season_scorer(String id_team_season_scorer) {
        this.id_team_season_scorer = id_team_season_scorer;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }
}
