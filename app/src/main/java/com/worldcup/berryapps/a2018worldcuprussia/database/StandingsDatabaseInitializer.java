
package com.worldcup.berryapps.a2018worldcuprussia.database;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;

import java.util.List;

public class StandingsDatabaseInitializer {

    private static final String TAG = StandingsDatabaseInitializer.class.getName();

    public static void populateAsync(@NonNull final Database db, List<Standings> standings) {
        PopulateDbAsync task = new PopulateDbAsync(db, standings);
        task.execute();
    }

    public static void populateSync(@NonNull final Database db, List<Standings> standings) {
        addStandings(db, standings);
    }

    private static List<Standings> addStandings(final Database db, List<Standings> standings) {
        standings.forEach(v -> db.standingsDao().insertAll(v));
        return standings;
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final Database mDb;
        private List<Standings> mStandings;

        PopulateDbAsync(Database db, List<Standings> standings) {
            mDb = db;
            mStandings = standings;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            addStandings(mDb, mStandings);
            return null;
        }

    }
}