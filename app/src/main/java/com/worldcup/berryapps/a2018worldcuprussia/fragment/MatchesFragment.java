package com.worldcup.berryapps.a2018worldcuprussia.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.SectionsPageAdapter;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.LiveTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.ResultsTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.matchesTabs.UpcomingTabFragment;
import com.worldcup.berryapps.a2018worldcuprussia.parser.MatchesParser;

public class MatchesFragment extends Fragment {

    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;
    private MatchesParser mp = new MatchesParser();

    @Override
    public void onResume() {
        super.onResume();
        if (getFragmentManager() != null) {
            for (Fragment fragment : getFragmentManager().getFragments()) {
                if (fragment instanceof UpcomingTabFragment || fragment instanceof LiveTabFragment || fragment instanceof  ResultsTabFragment) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.detach(fragment);
                    ft.attach(fragment);
                    ft.commit();
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matches, container, false);
        mSectionsPageAdapter = new SectionsPageAdapter(getChildFragmentManager());
        mp.initJSONTask(getContext());
        // Set up the ViewPager with the sections adapter.
        mViewPager = view.findViewById(R.id.container_matches);
        mViewPager.setOffscreenPageLimit(2);
        setupViewPager(mViewPager);

        TabLayout tabLayout = view.findViewById(R.id.tabs_matches);
        tabLayout.setupWithViewPager(mViewPager);
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getFragmentManager());
        adapter.addFragment(new LiveTabFragment(), "Live");
        adapter.addFragment(new UpcomingTabFragment(), "Upcoming");
        adapter.addFragment(new ResultsTabFragment(), "Results");
        viewPager.setAdapter(adapter);
    }
}