package com.worldcup.berryapps.a2018worldcuprussia.database;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.worldcup.berryapps.a2018worldcuprussia.model.Fixture;

import java.util.List;

public class FixtureDatabaseInitializer {

    private static final String TAG = FixtureDatabaseInitializer.class.getName();

    public static void populateAsync(@NonNull final Database db, List<Fixture> fixtures) {
        FixtureDatabaseInitializer.PopulateDbAsync task = new FixtureDatabaseInitializer.PopulateDbAsync(db, fixtures);
        task.execute();
    }

    public static void populateSync(@NonNull final Database db, List<Fixture> fixtures) {
        addFixtures(db, fixtures);
    }

    private static List<Fixture> addFixtures(final Database db, List<Fixture> fixtures) {
        fixtures.forEach(v -> db.fixtureDao().insertAll(v));
        return fixtures;
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final Database mDb;
        private List<Fixture> mFixtures;

        PopulateDbAsync(Database db, List<Fixture> fixtures) {
            mDb = db;
            mFixtures = fixtures;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            addFixtures(mDb, mFixtures);
            return null;
        }

    }
}
