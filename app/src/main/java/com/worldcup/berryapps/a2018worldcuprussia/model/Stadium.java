package com.worldcup.berryapps.a2018worldcuprussia.model;

public class Stadium {

    private String name;
    private String location;
    private String capacity;
    private int thumbnail;

    public Stadium() {
    }

    public Stadium(String name, String location, String capacity, int thumbnail) {
        this.name = name;
        this.location = location;
        this.capacity = capacity;
        this.thumbnail = thumbnail;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
}
