package com.worldcup.berryapps.a2018worldcuprussia.detailedActivities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.worldcup.berryapps.a2018worldcuprussia.FlagThumbnailProcessor;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.activity.MainActivity;
import com.worldcup.berryapps.a2018worldcuprussia.model.Lineups;
import com.worldcup.berryapps.a2018worldcuprussia.token.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DetailedMatch extends AppCompatActivity {

    private String LineUpsUrlPart1 = "https://api.sportdeer.com/v1/fixtures/";
    private String LineUpsUrlPart2 = "/lineups?access_token=";
    private List<String> standingsUrl = new ArrayList<>();
    private String URL_TO_HIT;
    private ListView listHomeLineUps, listHomeMatchDetail, listAwayMatchDetail;
    private ListView listAwayLineUps;
    private final String refreshToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YWZjMmYyMDMyZGI0NTY5Njc0MDMxN2IiLCJpYXQiOjE1MjgxODgzMjd9.35xB8Gq8GXrvrSZyFDElnGGgblpkOwEJnDO8KRdhvhw";
    private String accessToken;
    private List<Lineups> collect = new ArrayList<>();
    private String fixtureId = "";
    private String awayTeamString;
    private String homeTeamString;
    private LinearLayout linearLayoutLineUp;
    private LinearLayout linearLayoutLineUpSign;
    private LinearLayout linearLayoutMatchDetails;
    private LinearLayout linearLayoutMatchDetailsSign;
    HashMap<String, String> teamPlayers = new HashMap<>();
    private ImageView backButton;
    private ImageView awayTeamThumbnail;
    private ImageView homeTeamThumbnail;
    private TextView stadiumName;
    private TextView awayTeam;
    private TextView homeTeam;
    private TextView date;
    private TextView scoreHomeTeam;
    private TextView scoreAwayTeam;
    private RelativeLayout rl;

    public DetailedMatch() {
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_match);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setUpUIViews();

        backButton.setOnClickListener(v -> {
            finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            awayTeamString = bundle.getString("awayTeam"); // getting the model from MainActivity send via extras
            homeTeamString = bundle.getString("homeTeam"); // getting the model from MainActivity send via extras
            String dateString = bundle.getString("date"); // getting the model from MainActivity send via extras
            String awayScore = bundle.getString("awayScore");
            String homeScore = bundle.getString("homeScore");
            String stadium = bundle.getString("stadiumName");
            String fixtureStatus = bundle.getString("fixtureStatus");
            fixtureId = bundle.getString("fixtureId");

            if (!fixtureStatus.equals("NS")) {
                parseLineUps();
                linearLayoutLineUpSign.setVisibility(View.GONE);
                linearLayoutLineUp.setVisibility(View.VISIBLE);


                ParseMatchDetails parseMatchDetails = new ParseMatchDetails();
                parseMatchDetails.parse(getApplicationContext(), teamPlayers, fixtureId, listHomeMatchDetail, listAwayMatchDetail, awayTeamString, homeTeamString);
                linearLayoutMatchDetailsSign.setVisibility(View.GONE);
                linearLayoutMatchDetails.setVisibility(View.VISIBLE);

            }

            String parsedDate = parseDateTime(dateString);

            assert fixtureStatus != null;
            if (!fixtureStatus.equals("NS")) {
                scoreAwayTeam.setText(awayScore);
                scoreHomeTeam.setText(homeScore);
            }
            awayTeam.setText(awayTeamString);
            homeTeam.setText(homeTeamString);
            stadiumName.setText("Stadium: " + stadium);
            date.setText("Kick Off: " + parsedDate);
            FlagThumbnailProcessor ftp = new FlagThumbnailProcessor();
            if (awayTeamString != null) {
                awayTeamThumbnail.setImageResource(ftp.processTeamThumbnail(awayTeamString));
            }
            if (homeTeamString != null) {
                homeTeamThumbnail.setImageResource(ftp.processTeamThumbnail(homeTeamString));
            }
        }

    }


    private void parseLineUps() {
        listHomeLineUps = findViewById(R.id.listHomeLineUps);
        listAwayLineUps = findViewById(R.id.listAwayLineUps);
        listHomeMatchDetail = findViewById(R.id.listHomeMatchDetails);
        listAwayMatchDetail = findViewById(R.id.listAwayMatchDetails);
        linearLayoutLineUpSign = findViewById(R.id.layoutLineUpSign);
        linearLayoutLineUp = findViewById(R.id.layoutLineUp);
        linearLayoutMatchDetailsSign = findViewById(R.id.layoutMatchDetailsSign);
        linearLayoutMatchDetails = findViewById(R.id.layoutMatchDetails);

        new JSONTask().execute(URL_TO_HIT);

    }

    public class JSONTask extends AsyncTask<String, String, List<Lineups>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            collect.clear();
        }

        @Override
        protected List<Lineups> doInBackground(String... params) {
            List<Lineups> lineupsList = new ArrayList<>();


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            //Get Access Token
            accessToken = "https://api.sportdeer.com/v1/accessToken?refresh_token=" + refreshToken;
            AccessToken at = new AccessToken();
            accessToken = at.createAccessToken(accessToken);
            URL_TO_HIT = LineUpsUrlPart1 + fixtureId + LineUpsUrlPart2 + accessToken;


            try {
                URL url = new URL(URL_TO_HIT);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                JSONArray parentArray = parentObject.getJSONArray("docs");


                Gson gson = new Gson();
                for (int j = 0; j < parentArray.length(); j++) {
                    JSONObject finalObject = parentArray.getJSONObject(j);

                    Lineups lineUps = gson.fromJson(finalObject.toString(), Lineups.class);
                    lineupsList.add(lineUps);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            for (Lineups i : lineupsList) {
                teamPlayers.put(i.getId_team_season_player(), i.getPlayer_name());
            }
            lineupsList.stream().sorted(Comparator.comparing(Lineups::getIs_startingXI)).distinct();
            return lineupsList;
        }

        @Override
        protected void onPostExecute(final List<Lineups> result) {
            super.onPostExecute(result);
            if (result != null) {
                List<Lineups> awayTeamLineUp = new ArrayList<>();
                List<Lineups> homeTeamLineUp = new ArrayList<>();

                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i).getTeam_name().equals(awayTeamString)) {
                        awayTeamLineUp.add(result.get(i));
                    }
                }
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i).getTeam_name().equals(homeTeamString)) {
                        homeTeamLineUp.add(result.get(i));
                    }
                }

                HomeTeamLineUpAdapter homeTeamLineUpAdapter = new HomeTeamLineUpAdapter(getApplicationContext(), R.layout.row_home_team, homeTeamLineUp);
                listHomeLineUps.setAdapter(homeTeamLineUpAdapter);
                AwayTeamLineUpAdapter awayTeamLineUpAdapter = new AwayTeamLineUpAdapter(getApplicationContext(), R.layout.row_away_team, awayTeamLineUp);
                listAwayLineUps.setAdapter(awayTeamLineUpAdapter);
            } else {
                Toast.makeText(getApplicationContext().getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String parseDateTime(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        String dateStr = null;
        try {
            Date date = dateFormat.parse(dateString);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            dateStr = formatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateStr;
    }

    private void setUpUIViews() {
        rl = findViewById(R.id.back);
        backButton = findViewById(R.id.backButton);
        awayTeamThumbnail = (ImageView) findViewById(R.id.awayTeamThumbnailDetail);
        homeTeamThumbnail = (ImageView) findViewById(R.id.homeTeamThumbnailDetail);
        stadiumName = (TextView) findViewById(R.id.stadiumDetail);
        date = (TextView) findViewById(R.id.dateDetail);
        awayTeam = (TextView) findViewById(R.id.awayTeamDetail);
        homeTeam = (TextView) findViewById(R.id.homeTeamDetail);
        scoreHomeTeam = (TextView) findViewById(R.id.textViewScoreHomeDetail);
        scoreAwayTeam = (TextView) findViewById(R.id.textViewScoreAwayDetail);
    }

    public class AwayTeamLineUpAdapter extends ArrayAdapter {

        private List<Lineups> lineUpsList;
        private int resource;
        private LayoutInflater inflater;

        public AwayTeamLineUpAdapter(Context context, int resource, List<Lineups> objects) {
            super(context, resource, objects);
            lineUpsList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            AwayTeamLineUpAdapter.ViewHolder holder = null;

            if (convertView == null) {
                holder = new AwayTeamLineUpAdapter.ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.awayPlayerName = (TextView) convertView.findViewById(R.id.awayPlayerNameLineUp);
                holder.awayShirtNumber = convertView.findViewById(R.id.awayShirtNumberLineUp);
                convertView.setTag(holder);
            } else {
                holder = (AwayTeamLineUpAdapter.ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final AwayTeamLineUpAdapter.ViewHolder finalHolder = holder;
            holder.awayPlayerName.setText(lineUpsList.get(position).getPlayer_name());
            holder.awayShirtNumber.setText(lineUpsList.get(position).getShirtNumber());

            return convertView;
        }

        class ViewHolder {
            private TextView awayPlayerName;
            private TextView awayShirtNumber;
        }

    }

    public class HomeTeamLineUpAdapter extends ArrayAdapter {

        private List<Lineups> lineUpsList;
        private int resource;
        private LayoutInflater inflater;

        public HomeTeamLineUpAdapter(Context context, int resource, List<Lineups> objects) {
            super(context, resource, objects);
            lineUpsList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            HomeTeamLineUpAdapter.ViewHolder holder = null;

            if (convertView == null) {
                holder = new HomeTeamLineUpAdapter.ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.homePlayerName = (TextView) convertView.findViewById(R.id.homePlayerNameLineUp);
                holder.homeShirtNumber = convertView.findViewById(R.id.homeShirtNumberLineUp);
                convertView.setTag(holder);
            } else {
                holder = (HomeTeamLineUpAdapter.ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final HomeTeamLineUpAdapter.ViewHolder finalHolder = holder;
            holder.homePlayerName.setText(lineUpsList.get(position).getPlayer_name());
            holder.homeShirtNumber.setText(lineUpsList.get(position).getShirtNumber());

            return convertView;
        }

        class ViewHolder {
            private TextView homePlayerName;
            private TextView homeShirtNumber;
        }

    }
}
