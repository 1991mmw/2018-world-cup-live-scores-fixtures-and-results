package com.worldcup.berryapps.a2018worldcuprussia.matchesTabs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.worldcup.berryapps.a2018worldcuprussia.FlagThumbnailProcessor;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.database.Database;
import com.worldcup.berryapps.a2018worldcuprussia.detailedActivities.DetailedMatch;
import com.worldcup.berryapps.a2018worldcuprussia.model.Fixture;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ResultsTabFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView resultInfo;
    private ListView listFixtures;
    private UpcomingTabFragment.OnFragmentInteractionListener mListener;

    public ResultsTabFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static UpcomingTabFragment newInstance(String param1, String param2) {
        UpcomingTabFragment fragment = new UpcomingTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.results_tab_fragment, container, false);
        listFixtures = view.findViewById(R.id.listLive);
        resultInfo = view.findViewById(R.id.resultsInfo);

        List<Fixture> result = Database.getDatabase(getContext()).fixtureDao().getAll();

        if(result != null) {
            List<Fixture> finishedFixtures = new ArrayList<>();
            for(int i = 0; i < result.size(); i++) {
                if(result.get(i).getFixture_status_short().equals("FIN")) {
                    finishedFixtures.add(result.get(i));
                }
            }
            if(finishedFixtures.size()>0) {
                resultInfo.setVisibility(View.INVISIBLE);
            } else{
                resultInfo.setVisibility(View.VISIBLE);
            }
            ResultsTabFragment.FixtureAdapter adapter = new ResultsTabFragment.FixtureAdapter(getActivity(), R.layout.row, finishedFixtures);
            listFixtures.setAdapter(adapter);

            listFixtures.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                // list item click opens a new detailed activity
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Fixture fixture = finishedFixtures.get(position);
                    Intent intent = new Intent(getContext(), DetailedMatch.class);
                    intent.putExtra("awayTeam", fixture.getTeam_season_away_name());
                    intent.putExtra("homeTeam", fixture.getTeam_season_home_name());
                    if(!fixture.getFixture_status_short().equals("NS")) {
                        intent.putExtra("awayScore", fixture.getNumber_goal_team_away());
                        intent.putExtra("homeScore", fixture.getNumber_goal_team_home());
                    }
                    intent.putExtra("date", fixture.getSchedule_date());
                    intent.putExtra("stadiumName", fixture.getStadium());
                    intent.putExtra("fixtureStatus", fixture.getFixture_status_short());
                    intent.putExtra("fixtureId", fixture.get_id());
                    getContext().startActivity(intent);
                }
            });
        } else {
            Toast.makeText(getContext().getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class FixtureAdapter extends ArrayAdapter {

        private List<Fixture> fixtureList;
        private int resource;
        private LayoutInflater inflater;
        public FixtureAdapter(Context context, int resource, List<Fixture> objects) {
            super(context, resource, objects);
            fixtureList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ResultsTabFragment.FixtureAdapter.ViewHolder holder = null;

            if(convertView == null){
                holder = new ResultsTabFragment.FixtureAdapter.ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.awayTeam = (TextView)convertView.findViewById(R.id.awayTeam);
                holder.homeTeam = (TextView)convertView.findViewById(R.id.homeTeam);
                holder.awayTeamThumbnail = (ImageView) convertView.findViewById(R.id.awayTeamThumbnail);
                holder.homeTeamThumbnail = (ImageView)convertView.findViewById(R.id.homeTeamThumbnail);
                holder.date = (TextView)convertView.findViewById(R.id.date);
                holder.stadium = (TextView) convertView.findViewById(R.id.stadium);
                holder.scoreAwayTeam = (TextView) convertView.findViewById(R.id.textViewScoreAway);
                holder.scoreHomeTeam = (TextView) convertView.findViewById(R.id.textViewScoreHome);

                convertView.setTag(holder);
            } else {
                holder = (ResultsTabFragment.FixtureAdapter.ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar)convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final ResultsTabFragment.FixtureAdapter.ViewHolder finalHolder = holder;

            String dateString = fixtureList.get(position).getSchedule_date().toString();
            FlagThumbnailProcessor ftp = new FlagThumbnailProcessor();
            String parsedDate = ftp.parseDateTime(dateString);

            holder.homeTeam.setText(fixtureList.get(position).getTeam_season_home_name().toUpperCase());
            holder.awayTeam.setText(fixtureList.get(position).getTeam_season_away_name().toUpperCase());
            Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"fonts/dusha.ttf");
            holder.homeTeam.setTypeface(type);
            holder.awayTeam.setTypeface(type);

            holder.date.setText("Kick Off: " + parsedDate);

            holder.stadium.setText("Stadium: " + fixtureList.get(position).getStadium());

            holder.awayTeamThumbnail.setImageResource(ftp.processTeamThumbnail(fixtureList.get(position).getTeam_season_away_name()));
            holder.homeTeamThumbnail.setImageResource(ftp.processTeamThumbnail(fixtureList.get(position).getTeam_season_home_name()));

            holder.scoreHomeTeam.setText(fixtureList.get(position).getNumber_goal_team_home());
            holder.scoreAwayTeam.setText(fixtureList.get(position).getNumber_goal_team_away());

            return convertView;
        }

        class ViewHolder{

            private ImageView awayTeamThumbnail;
            private ImageView homeTeamThumbnail;
            private TextView date;
            private TextView awayTeam;
            private TextView homeTeam;
            private TextView stadium;
            private TextView scoreAwayTeam;
            private TextView scoreHomeTeam;
        }

    }
}
