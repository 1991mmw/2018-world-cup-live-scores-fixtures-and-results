package com.worldcup.berryapps.a2018worldcuprussia.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "fixture")
public class Fixture {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "fixture_status")
    private String fixture_status;

    @ColumnInfo(name = "schedule_date")
    private String schedule_date;

    @ColumnInfo(name = "team_season_away_name")
    private String team_season_away_name;

    @ColumnInfo(name = "team_season_home_name")
    private String team_season_home_name;

    @ColumnInfo(name = "round")
    private String round;

    @ColumnInfo(name = "stadium")
    private String stadium;

    @ColumnInfo(name = "awayTeamThumbnail")
    private String awayTeamThumbnail;

    @ColumnInfo(name = "homeTeamThumbnail")
    private String homeTeamThumbnail;

    @ColumnInfo(name = "fixture_status_short")
    private String fixture_status_short;

    @ColumnInfo(name = "number_goal_team_away")
    private String number_goal_team_away;

    @ColumnInfo(name = "number_goal_team_home")
    private String number_goal_team_home;

    @ColumnInfo(name = "_id")
    private String _id;


    public Fixture() {
    }

    public Fixture(int uid, String fixture_status, String schedule_date, String team_season_away_name,
                   String team_season_home_name, String round, String stadium, String awayTeamThumbnail,
                   String homeTeamThumbnail, String fixture_status_short, String number_goal_team_away, String number_goal_team_home, String _id) {
        this.uid = uid;
        this.fixture_status = fixture_status;
        this.schedule_date = schedule_date;
        this.team_season_away_name = team_season_away_name;
        this.team_season_home_name = team_season_home_name;
        this.round = round;
        this.stadium = stadium;
        this.awayTeamThumbnail = awayTeamThumbnail;
        this.homeTeamThumbnail = homeTeamThumbnail;
        this.fixture_status_short = fixture_status_short;
        this.number_goal_team_away = number_goal_team_away;
        this.number_goal_team_home = number_goal_team_home;
        this._id = _id;

    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNumber_goal_team_away() {
        return number_goal_team_away;
    }

    public void setNumber_goal_team_away(String number_goal_team_away) {
        this.number_goal_team_away = number_goal_team_away;
    }

    public String getNumber_goal_team_home() {
        return number_goal_team_home;
    }

    public void setNumber_goal_team_home(String number_goal_team_home) {
        this.number_goal_team_home = number_goal_team_home;
    }

    public String getFixture_status_short() {
        return fixture_status_short;
    }

    public void setFixture_status_short(String fixture_status_short) {
        this.fixture_status_short = fixture_status_short;
    }

    public String getFixture_status() {
        return fixture_status;
    }

    public void setFixture_status(String fixture_status) {
        this.fixture_status = fixture_status;
    }

    public String getSchedule_date() {
        return schedule_date;
    }

    public void setSchedule_date(String schedule_date) {
        this.schedule_date = schedule_date;
    }

    public String getTeam_season_away_name() {
        return team_season_away_name;
    }

    public void setTeam_season_away_name(String team_season_away_name) {
        this.team_season_away_name = team_season_away_name;
    }

    public String getTeam_season_home_name() {
        return team_season_home_name;
    }

    public void setTeam_season_home_name(String team_season_home_name) {
        this.team_season_home_name = team_season_home_name;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public String getAwayTeamThumbnail() {
        return awayTeamThumbnail;
    }

    public void setAwayTeamThumbnail(String awayTeamThumbnail) {
        this.awayTeamThumbnail = awayTeamThumbnail;
    }

    public String getHomeTeamThumbnail() {
        return homeTeamThumbnail;
    }

    public void setHomeTeamThumbnail(String homeTeamThumbnail) {
        this.homeTeamThumbnail = homeTeamThumbnail;
    }
}
