package com.worldcup.berryapps.a2018worldcuprussia.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;

import java.util.List;

@Dao
public interface StandingsDao {

    @Insert
    void insertAll(Standings... standings);

    @Delete
    void delete(Standings standings);

    @Query("SELECT * FROM standings")
    List<Standings> getAll();

    @Query("DELETE FROM standings")
    void nukeTable();
}
