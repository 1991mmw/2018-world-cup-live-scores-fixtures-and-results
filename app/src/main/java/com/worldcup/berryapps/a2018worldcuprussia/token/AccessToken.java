package com.worldcup.berryapps.a2018worldcuprussia.token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AccessToken {

    public String createAccessToken(String accessToken) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(accessToken);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();

            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            String finalJson = buffer.toString();

            JSONObject parentObject = new JSONObject(finalJson);

            Object new_access_token = parentObject.get("new_access_token");
            accessToken = (String) new_access_token;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return accessToken;
    }

}
