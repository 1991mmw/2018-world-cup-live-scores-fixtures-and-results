package com.worldcup.berryapps.a2018worldcuprussia;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FlagThumbnailProcessor {

    public int processTeamThumbnail(String team) {
        if(team.equals("Argentina")) {
            return R.drawable.flag0argentina;
        }
        if(team.equals("Australia")) {
            return R.drawable.flag0australia;
        }
        if(team.equals("Belgium")) {
            return R.drawable.flag0belgium;
        }
        if(team.equals("Brazil")) {
            return R.drawable.flag0brazil;
        }
        if(team.equals("Colombia")) {
            return R.drawable.flag0colombia;
        }
        if(team.equals("Costa Rica")) {
            return R.drawable.flag0costa_rica;
        }
        if(team.equals("Croatia")) {
            return R.drawable.flag0croatia;
        }
        if(team.equals("Denmark")) {
            return R.drawable.flag0denmark;
        }
        if(team.equals("Egypt")) {
            return R.drawable.flag0egypt;
        }
        if(team.equals("England")) {
            return R.drawable.flag0england;
        }
        if(team.equals("France")) {
            return R.drawable.flag0france;
        }
        if(team.equals("Germany")) {
            return R.drawable.flag0germany;
        }
        if(team.equals("Iceland")) {
            return R.drawable.flag0iceland;
        }
        if(team.equals("Iran")) {
            return R.drawable.flag0iran;
        }
        if(team.equals("Japan")) {
            return R.drawable.flag0japan;
        }
        if(team.equals("Mexico")) {
            return R.drawable.flag0mexico;
        }
        if(team.equals("Morocco")) {
            return R.drawable.flag0morocco;
        }
        if(team.equals("Nigeria")) {
            return R.drawable.flag0nigeria;
        }
        if(team.equals("Panama")) {
            return R.drawable.flag0panama;
        }
        if(team.equals("Peru")) {
            return R.drawable.flag0peru;
        }
        if(team.equals("Poland")) {
            return R.drawable.flag0poland;
        }
        if(team.equals("Portugal")) {
            return R.drawable.flag0portugal;
        }
        if(team.equals("Russia")) {
            return R.drawable.flag0russia;
        }
        if(team.equals("Saudi Arabia")) {
            return R.drawable.flag0saudi_arabia;
        }
        if(team.equals("Senegal")) {
            return R.drawable.flag0senegal;
        }
        if(team.equals("Serbia")) {
            return R.drawable.flag0serbia;
        }
        if(team.equals("South Korea")) {
            return R.drawable.flag0south_korea;
        }
        if(team.equals("Spain")) {
            return R.drawable.flag0spain;
        }
        if(team.equals("Sweden")) {
            return R.drawable.flag0sweden;
        }
        if(team.equals("Switzerland")) {
            return R.drawable.flag0switzerland;
        }
        if(team.equals("Tunisia")) {
            return R.drawable.flag0tunisia;
        }
        if(team.equals("Uruguay")) {
            return R.drawable.flag0uruguay;
        }
        return R.drawable.cover_2;
    }

    public int processStadiumImage(String stadiumName) {
        if (stadiumName.equals("Kalingrad Stadium")) {
            return R.drawable.kaliningrad;
        }
        if (stadiumName.equals("Kazan Arena")) {
            return R.drawable.kazan;
        }
        if (stadiumName.equals("Luzhniki Stadium")) {
            return R.drawable.luzhniki;
        }
        if (stadiumName.equals("Spartak Stadium")) {
            return R.drawable.spartak;
        }
        if (stadiumName.equals("Nizhny Novgorod Stadium")) {
            return R.drawable.nizhny_novograd;
        }
        if (stadiumName.equals("Rostov Arena")) {
            return R.drawable.rostov_on_don;
        }
        if (stadiumName.equals("Krestovsky Stadium")) {
            return R.drawable.saint_petersburg;
        }
        if (stadiumName.equals("Cosmos Arena")) {
            return R.drawable.samara;
        }
        if (stadiumName.equals("Mordovia Arena")) {
            return R.drawable.saransl;
        }
        if (stadiumName.equals("Fisht Stadium")) {
            return R.drawable.sochi;
        }
        if (stadiumName.equals("Volgograd Arena")) {
            return R.drawable.volgograd;
        }
        if (stadiumName.equals("Central Stadium")) {
            return R.drawable.yekaterinburg;
        }
        return R.drawable.cover;
    }

    public String parseDateTime(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        String dateStr = null;
        try {
            Date date = dateFormat.parse(dateString);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            dateStr = formatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateStr;
    }
}
