package com.worldcup.berryapps.a2018worldcuprussia.model;

public class Lineups {

    private String _id;
    private String shirtNumber;
    private String is_startingXI;
    private String team_name;
    private String player_name;
    private String id_team_season_player;

    public Lineups() {
    }

    public Lineups(String _id, String shirtNumber, String is_startingXI, String team_name, String player_name, String id_team_season_player) {
        this._id = _id;
        this.shirtNumber = shirtNumber;
        this.is_startingXI = is_startingXI;
        this.team_name = team_name;
        this.player_name = player_name;
        this.id_team_season_player = id_team_season_player;
    }

    public String getId_team_season_player() {
        return id_team_season_player;
    }

    public void setId_team_season_player(String id_team_season_player) {
        this.id_team_season_player = id_team_season_player;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(String shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public String getIs_startingXI() {
        return is_startingXI;
    }

    public void setIs_startingXI(String is_startingXI) {
        this.is_startingXI = is_startingXI;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getPlayer_name() {
        return player_name;
    }

    public void setPlayer_name(String player_name) {
        this.player_name = player_name;
    }
}
