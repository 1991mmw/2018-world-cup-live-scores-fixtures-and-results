package com.worldcup.berryapps.a2018worldcuprussia.database;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.worldcup.berryapps.a2018worldcuprussia.dao.FixtureDao;
import com.worldcup.berryapps.a2018worldcuprussia.dao.StandingsDao;
import com.worldcup.berryapps.a2018worldcuprussia.model.Fixture;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;

@android.arch.persistence.room.Database(entities = {Standings.class, Fixture.class}, version = 2)
public abstract class Database extends RoomDatabase {

    private static Database INSTANCE;

    public abstract StandingsDao standingsDao();

    public abstract FixtureDao fixtureDao();

    public static Database getDatabase(Context context) {
        if(INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    Database.class, "database")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }


}
