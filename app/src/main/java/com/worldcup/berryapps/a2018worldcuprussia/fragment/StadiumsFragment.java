package com.worldcup.berryapps.a2018worldcuprussia.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.model.Stadium;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.StadiumsAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StadiumsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StadiumsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StadiumsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private StadiumsAdapter adapter;
    private List<Stadium> stadiumsList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private OnFragmentInteractionListener mListener;

    public StadiumsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StadiumsFragment newInstance(String param1, String param2) {
        StadiumsFragment fragment = new StadiumsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stadiums, container, false);
        // Inflate the layout for this fragment
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        initCollapsingToolbar(view);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        stadiumsList = new ArrayList<>();
        adapter = new StadiumsAdapter(getContext(), stadiumsList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new StadiumsFragment.GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareStadiums();

        try {
            Glide.with(this).load(R.drawable.cover).into((ImageView) view.findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar(View view) {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Adding stadiums
     */
    private void prepareStadiums() {
        int[] stadiums = new int[]{
                R.drawable.kaliningrad,
                R.drawable.kazan,
                R.drawable.luzhniki,
                R.drawable.spartak,
                R.drawable.nizhny_novograd,
                R.drawable.rostov_on_don,
                R.drawable.saint_petersburg,
                R.drawable.samara,
                R.drawable.saransl,
                R.drawable.sochi,
                R.drawable.volgograd,
                R.drawable.yekaterinburg,
        };

        Stadium a = new Stadium("Kalingrad Stadium", "Kalingrad", "35,212", stadiums[0]);
        stadiumsList.add(a);

        a = new Stadium("Kazan Arena", "Kazan", "45,379" , stadiums[1]);
        stadiumsList.add(a);

        a = new Stadium("Luzhniki Stadium", "Moscow", "81,000", stadiums[2]);
        stadiumsList.add(a);

        a = new Stadium("Spartak Stadiun", "Moscow", "45,360", stadiums[3]);
        stadiumsList.add(a);

        a = new Stadium("Nizhny Novgorod Stadium", "Nizhny Novgorod", "44,899", stadiums[4]);
        stadiumsList.add(a);

        a = new Stadium("Rostov Arena", "Rostov-on-Don", "45,000", stadiums[5]);
        stadiumsList.add(a);

        a = new Stadium("Krestovsky Stadium", "Saint Petersburg", "68,134", stadiums[6]);
        stadiumsList.add(a);

        a = new Stadium("Cosmos Arena", "Samara", "44,918", stadiums[7]);
        stadiumsList.add(a);

        a = new Stadium("Mordovia Arena", "Saransk", "44,442", stadiums[8]);
        stadiumsList.add(a);

        a = new Stadium("Fisht Stadium", "Sochi", "47,659", stadiums[9]);
        stadiumsList.add(a);

        a = new Stadium("Volgograd Arena", "Volgograd", "45,568", stadiums[10]);
        stadiumsList.add(a);

        a = new Stadium("Central Stadium", "Yekaterinburg", "35,969", stadiums[11]);
        stadiumsList.add(a);

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
