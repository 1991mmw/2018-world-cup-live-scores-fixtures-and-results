package com.worldcup.berryapps.a2018worldcuprussia.detailedActivities;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.model.Event;
import com.worldcup.berryapps.a2018worldcuprussia.model.Lineups;
import com.worldcup.berryapps.a2018worldcuprussia.token.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

class ParseMatchDetails {

    private String LineUpsUrlPart1 = "https://api.sportdeer.com/v1/fixtures/";
    private String LineUpsUrlPart2 = "/events?access_token=";
    private List<String> standingsUrl = new ArrayList<>();
    private String URL_TO_HIT;
    private ListView listHomeLineUps, listHomeMatchDetails, listAwayMatchDetails;
    private ListView listAwayLineUps;
    private final String refreshToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YWZjMmYyMDMyZGI0NTY5Njc0MDMxN2IiLCJpYXQiOjE1MjgxODgzMjd9.35xB8Gq8GXrvrSZyFDElnGGgblpkOwEJnDO8KRdhvhw";
    private String accessToken;
    private List<Lineups> collect = new ArrayList<>();
    private String fixtureId;
    private String awayTeamString;
    private String homeTeamString;

    private Context mContext;
    private HashMap<String, String> teamPlayers;


    void parse(Context context, HashMap<String, String> hashMap, String fixture, ListView listHomeMatchDetail, ListView listAwayMatchDetail, String awayTeam, String homeTeam) {
        mContext = context;
        fixtureId = fixture;
        teamPlayers = hashMap;
        listAwayMatchDetails = listAwayMatchDetail;
        listHomeMatchDetails = listHomeMatchDetail;
        awayTeamString = awayTeam;
        homeTeamString = homeTeam;
        new JSONTask().execute(URL_TO_HIT);
    }

    public class JSONTask extends AsyncTask<String, String, List<Event>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            collect.clear();
        }

        @Override
        protected List<Event> doInBackground(String... params) {
            List<Event> events = new ArrayList<>();


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            //Get Access Token
            accessToken = "https://api.sportdeer.com/v1/accessToken?refresh_token=" + refreshToken;
            AccessToken at = new AccessToken();
            accessToken = at.createAccessToken(accessToken);
            URL_TO_HIT = LineUpsUrlPart1 + fixtureId + LineUpsUrlPart2 + accessToken;


            try {
                URL url = new URL(URL_TO_HIT);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                JSONArray parentArray = parentObject.getJSONArray("docs");


                Gson gson = new Gson();
                for (int j = 0; j < parentArray.length(); j++) {
                    JSONObject finalObject = parentArray.getJSONObject(j);

                    Event event = gson.fromJson(finalObject.toString(), Event.class);
                    events.add(event);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            events = events.stream().filter(type -> type.getType().equals("card") || type.getType().equals("goal")).collect(Collectors.toList());
            events.stream().sorted(Comparator.comparing(Event::getElapsed));

            return events;
        }

        @Override
        protected void onPostExecute(final List<Event> result) {
            super.onPostExecute(result);
            if (result != null) {
                List<Event> awayTeamMatchDetail = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i).getTeam_name().equals(awayTeamString)) {
                        awayTeamMatchDetail.add(result.get(i));
                    }
                }
                List<Event> homeTeamMatchDetail = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i).getTeam_name().equals(homeTeamString)) {
                        homeTeamMatchDetail.add(result.get(i));
                    }
                }
                AwayMatchDetailsAdapter awayMatchDetailsAdapter = new AwayMatchDetailsAdapter(mContext.getApplicationContext(), R.layout.row_away_match_details, awayTeamMatchDetail);
                listAwayMatchDetails.setAdapter(awayMatchDetailsAdapter);
                HomeMatchDetailsAdapter homeMatchDetailsAdapter = new HomeMatchDetailsAdapter(mContext.getApplicationContext(), R.layout.row_home_match_details, homeTeamMatchDetail);
                listHomeMatchDetails.setAdapter(homeMatchDetailsAdapter);
            } else {
                Toast.makeText(mContext.getApplicationContext().getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class AwayMatchDetailsAdapter extends ArrayAdapter {

        private List<Event> eventList;
        private int resource;
        private LayoutInflater inflater;

        public AwayMatchDetailsAdapter(Context context, int resource, List<Event> objects) {
            super(context, resource, objects);
            eventList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ParseMatchDetails.AwayMatchDetailsAdapter.ViewHolder holder = null;

            if (convertView == null) {
                holder = new ParseMatchDetails.AwayMatchDetailsAdapter.ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.elapsedView = (TextView) convertView.findViewById(R.id.awayElapsedTime);
                holder.typeImage = convertView.findViewById(R.id.awayTypeImage);
                holder.playerName = convertView.findViewById(R.id.awayPlayerName);
                convertView.setTag(holder);
            } else {
                holder = (ParseMatchDetails.AwayMatchDetailsAdapter.ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final ParseMatchDetails.AwayMatchDetailsAdapter.ViewHolder finalHolder = holder;
            holder.elapsedView.setText(eventList.get(position).getElapsed() + "'");
            String type = eventList.get(position).getType();
            String card_type = eventList.get(position).getCard_type();
            holder.typeImage.setImageResource(processEventType(type, card_type));

            String player = teamPlayers.get(eventList.get(position).getId_team_season_scorer());

            holder.playerName.setText(player);

            return convertView;
        }

        private int processEventType(String type, String card_type) {
            if (type.equals("goal")) {
                return R.drawable.ball;
            } else if (type.equals("card")) {
                if (card_type.equals("y")) {
                    return R.drawable.yellow_card;
                }
                if (card_type.equals("r")) {
                    return R.drawable.red_card;
                }
            }
            return R.drawable.ic_notifications_black_24dp;
        }

        class ViewHolder {
            private TextView elapsedView;
            private ImageView typeImage;
            private TextView playerName;
        }

    }

    public class HomeMatchDetailsAdapter extends ArrayAdapter {

        private List<Event> eventList;
        private int resource;
        private LayoutInflater inflater;

        public HomeMatchDetailsAdapter(Context context, int resource, List<Event> objects) {
            super(context, resource, objects);
            eventList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ParseMatchDetails.HomeMatchDetailsAdapter.ViewHolder holder = null;

            if (convertView == null) {
                holder = new ParseMatchDetails.HomeMatchDetailsAdapter.ViewHolder();
                convertView = inflater.inflate(resource, null);
                holder.elapsedView = (TextView) convertView.findViewById(R.id.homeElapsedTime);
                holder.typeImage = convertView.findViewById(R.id.homeTypeImage);
                holder.playerName = convertView.findViewById(R.id.homePlayerName);
                convertView.setTag(holder);
            } else {
                holder = (ParseMatchDetails.HomeMatchDetailsAdapter.ViewHolder) convertView.getTag();
            }

            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            // Then later, when you want to display image
            final ParseMatchDetails.HomeMatchDetailsAdapter.ViewHolder finalHolder = holder;
            holder.elapsedView.setText(eventList.get(position).getElapsed() + "'");
            String type = eventList.get(position).getType();
            String card_type = eventList.get(position).getCard_type();
            holder.typeImage.setImageResource(processEventType(type, card_type));

            String player = teamPlayers.get(eventList.get(position).getId_team_season_scorer());

            holder.playerName.setText(player);

            return convertView;
        }

        private int processEventType(String type, String card_type) {
            if (type.equals("goal")) {
                return R.drawable.ball;
            } else if (type.equals("card")) {
                if (card_type.equals("y")) {
                    return R.drawable.yellow_card;
                }
                if (card_type.equals("r")) {
                    return R.drawable.red_card;
                }
            }
            return R.drawable.ic_notifications_black_24dp;
        }

        class ViewHolder {
            private TextView elapsedView;
            private ImageView typeImage;
            private TextView playerName;
        }

    }
}
