package com.worldcup.berryapps.a2018worldcuprussia.model;

public class Team {
    private String name;
    private String group;
    private int thumbnail;

    public Team() {
    }

    public Team(String name, String group, int thumbnail) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
