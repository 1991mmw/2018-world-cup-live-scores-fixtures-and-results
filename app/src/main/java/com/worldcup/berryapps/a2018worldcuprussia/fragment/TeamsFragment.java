package com.worldcup.berryapps.a2018worldcuprussia.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.model.Team;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.TeamAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TeamsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeamsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeamsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private TeamAdapter adapter;
    private List<Team> teamList;

    private OnFragmentInteractionListener mListener;

    public TeamsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PhotosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TeamsFragment newInstance(String param1, String param2) {
        TeamsFragment fragment = new TeamsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teams, container, false);

        initCollapsingToolbar(view);

        recyclerView = view.findViewById(R.id.recycler_view);

        teamList = new ArrayList<>();
        adapter = new TeamAdapter(this.getContext(), teamList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new TeamsFragment.GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();

        try {
            Glide.with(this).load(R.drawable.cover_2).into((ImageView) view.findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initCollapsingToolbar(View view) {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Bla bla");
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        int[] flags = new int[]{
                R.drawable.flag0argentina,
                R.drawable.flag0australia,
                R.drawable.flag0belgium,
                R.drawable.flag0brazil,
                R.drawable.flag0colombia,
                R.drawable.flag0costa_rica,
                R.drawable.flag0croatia,
                R.drawable.flag0denmark,
                R.drawable.flag0egypt,
                R.drawable.flag0england,
                R.drawable.flag0france,
                R.drawable.flag0germany,
                R.drawable.flag0iceland,
                R.drawable.flag0iran,
                R.drawable.flag0japan,
                R.drawable.flag0mexico,
                R.drawable.flag0morocco,
                R.drawable.flag0nigeria,
                R.drawable.flag0panama,
                R.drawable.flag0peru,
                R.drawable.flag0poland,
                R.drawable.flag0portugal,
                R.drawable.flag0russia,
                R.drawable.flag0saudi_arabia,
                R.drawable.flag0senegal,
                R.drawable.flag0serbia,
                R.drawable.flag0south_korea,
                R.drawable.flag0spain,
                R.drawable.flag0sweden,
                R.drawable.flag0switzerland,
                R.drawable.flag0tunisia,
                R.drawable.flag0uruguay,


        };

        Team a = new Team("Argentina", "Group D", flags[0]);
        teamList.add(a);

        a = new Team("Australia", "Group C", flags[1]);
        teamList.add(a);

        a = new Team("Belgium", "Group G", flags[2]);
        teamList.add(a);

        a = new Team("Brazil", "Group E", flags[3]);
        teamList.add(a);

        a = new Team("Colombia", "Group H", flags[4]);
        teamList.add(a);

        a = new Team("Costa Rica", "Group E", flags[5]);
        teamList.add(a);

        a = new Team("Croatia", "Group D", flags[6]);
        teamList.add(a);

        a = new Team("Denmark", "Group C", flags[7]);
        teamList.add(a);

        a = new Team("Egypt", "Group A", flags[8]);
        teamList.add(a);

        a = new Team("England", "Group G", flags[9]);
        teamList.add(a);

        a = new Team("France", "Group C", flags[10]);
        teamList.add(a);

        a = new Team("Germany", "Group F", flags[11]);
        teamList.add(a);

        a = new Team("Iceland", "Group D", flags[12]);
        teamList.add(a);

        a = new Team("Iran", "Group B", flags[13]);
        teamList.add(a);

        a = new Team("Japan", "Group H", flags[14]);
        teamList.add(a);

        a = new Team("Mexico", "Group F", flags[15]);
        teamList.add(a);

        a = new Team("Morocco", "Group B", flags[16]);
        teamList.add(a);

        a = new Team("Nigeria", "Group D", flags[17]);
        teamList.add(a);

        a = new Team("Panama", "Group G", flags[18]);
        teamList.add(a);

        a = new Team("Peru", "Group C", flags[19]);
        teamList.add(a);

        a = new Team("Poland", "Group H", flags[20]);
        teamList.add(a);

        a = new Team("Portugal", "Group B", flags[21]);
        teamList.add(a);

        a = new Team("Russia", "Group A", flags[22]);
        teamList.add(a);

        a = new Team("Saudi Arabia", "Group A", flags[23]);
        teamList.add(a);

        a = new Team("Senegal", "Group H", flags[24]);
        teamList.add(a);

        a = new Team("Serbia", "Group E", flags[25]);
        teamList.add(a);

        a = new Team("South Korea", "Group F", flags[26]);
        teamList.add(a);

        a = new Team("Spain", "Group B", flags[27]);
        teamList.add(a);

        a = new Team("Sweden", "Group F", flags[28]);
        teamList.add(a);

        a = new Team("Switzerland", "Group E", flags[29]);
        teamList.add(a);

        a = new Team("Tunisia", "Group G", flags[30]);
        teamList.add(a);

        a = new Team("Uruguay", "Group A", flags[31]);
        teamList.add(a);

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
