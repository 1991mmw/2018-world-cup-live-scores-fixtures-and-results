package com.worldcup.berryapps.a2018worldcuprussia.groupsTabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.worldcup.berryapps.a2018worldcuprussia.R;
import com.worldcup.berryapps.a2018worldcuprussia.adapters.StandingsAdapter;
import com.worldcup.berryapps.a2018worldcuprussia.database.Database;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by User on 2/28/2017.
 */

public class Tab1Fragment extends Fragment {
    private static final String TAG = "Tab1Fragment";
    private ListView listStandings;
    private List<Standings> groupA;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_fragment, container, false);
        listStandings = view.findViewById(R.id.listStandingsA);
        List<Standings> all = Database.getDatabase(getContext()).standingsDao().getAll();
        groupA = all.stream()
                .filter(v -> v.getTeam_season_name().equals("Russia") ||
                        v.getTeam_season_name().equals("Uruguay") ||
                        v.getTeam_season_name().equals("Saudi Arabia") ||
                        v.getTeam_season_name().equals("Egypt"))
                .sorted(Comparator.comparing(Standings::getPosition))
                .distinct()
                .collect(Collectors.toList());

        if (groupA != null) {
            Toast.makeText(getContext().getApplicationContext(), "Fetching data from server, please wait.", Toast.LENGTH_SHORT).show();
            StandingsAdapter adapter = new StandingsAdapter(getActivity(), R.layout.rows_standings, groupA);
            listStandings.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getContext().getApplicationContext(), "Not able to fetch data from server, please check url.", Toast.LENGTH_SHORT).show();
        }



        return view;
    }
}
