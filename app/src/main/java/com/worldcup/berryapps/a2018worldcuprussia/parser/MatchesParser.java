package com.worldcup.berryapps.a2018worldcuprussia.parser;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.worldcup.berryapps.a2018worldcuprussia.database.Database;
import com.worldcup.berryapps.a2018worldcuprussia.database.FixtureDatabaseInitializer;
import com.worldcup.berryapps.a2018worldcuprussia.database.StandingsDatabaseInitializer;
import com.worldcup.berryapps.a2018worldcuprussia.model.Fixture;
import com.worldcup.berryapps.a2018worldcuprussia.model.Standings;
import com.worldcup.berryapps.a2018worldcuprussia.token.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MatchesParser {

    private String URL_TO_HIT;
    private List<Fixture> collect = new ArrayList<>();
    private Context mContext;
    private List<String> fixturesUrls = new ArrayList<>();

    public void initJSONTask(Context context) {
        mContext = context;
        new JSONTask().execute(URL_TO_HIT);
    }

    public class JSONTask extends AsyncTask<String, String, List<Fixture>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fixturesUrls.clear();
            collect.clear();
        }

        @Override
        protected List<Fixture> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            String fixturesUrlPage1 = "https://api.sportdeer.com/v1/seasons/10/fixtures?page=1&access_token=";
            fixturesUrls.add(fixturesUrlPage1);
           /* String fixturesUrlPage2 = "https://api.sportdeer.com/v1/seasons/296/fixtures?page=2&access_token=";
            fixturesUrls.add(fixturesUrlPage2);
            String fixturesUrlPage3 = "https://api.sportdeer.com/v1/seasons/296/fixtures?page=3&access_token=";
            fixturesUrls.add(fixturesUrlPage3);*/


            //Get Access Token
            String refreshToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YWZjMmYyMDMyZGI0NTY5Njc0MDMxN2IiLCJpYXQiOjE1MjgxODgzMjd9.35xB8Gq8GXrvrSZyFDElnGGgblpkOwEJnDO8KRdhvhw";
            String accessToken = "https://api.sportdeer.com/v1/accessToken?refresh_token=" + refreshToken;
            AccessToken at = new AccessToken();
            accessToken = at.createAccessToken(accessToken);

            for(int i = 0; i < fixturesUrls.size(); i++) {

                URL_TO_HIT = fixturesUrls.get(i) + accessToken;


                try {
                    URL url = new URL(URL_TO_HIT);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    InputStream stream = connection.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }

                    String finalJson = buffer.toString();

                    JSONObject parentObject = new JSONObject(finalJson);
                    JSONArray parentArray = parentObject.getJSONArray("docs");


                    Gson gson = new Gson();
                    for (int j = 0; j < parentArray.length(); j++) {
                        JSONObject finalObject = parentArray.getJSONObject(j);

                        Fixture fixture = gson.fromJson(finalObject.toString(), Fixture.class);
                        collect.add(fixture);
                    }

                } catch (MalformedURLException | JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Database.getDatabase(mContext).fixtureDao().nukeTable();
            FixtureDatabaseInitializer.populateAsync(Database.getDatabase(mContext), collect);
            return collect;
        }

        @Override
        protected void onPostExecute(final List<Fixture> result) {
            super.onPostExecute(result);
        }
    }
}
